# Notes on the Gitea setup

## SSL 

Most importantly encrypt the data streams. Nginx is a good option for enforcing SSL traffic.

### Self-signed certificates

```bash
mkdir git-certs
cat raspberrytea.crt > ~/git-certs/cert.pem
git config --global http.sslCAInfo $HOME/git-certs/cert.pem
```

## Webhooks

Read about it here: 

```
https://dev.to/rrampage/webhook-to-auto-deploy-on-git-push-to-github-330i
```

## CI Options 

* [buildbot](https://docs.buildbot.net/current/tutorial/index.html) -> documentation is hard so I need to do my own probably
* [Woodpecker](https://woodpecker-ci.org/docs/intro) -> needs docker (is a nono)

## Better Podman Setup 


[Blog](https://blog.while-true-do.io/podman-setup-gitea/)


## Change url 

The file is in 

```
/etc/gitea/app.ini
```
Change the server configuration file from `localhost` to the private IP address. 

## Store creds 

Is unencrypted on harddrive, though 

`git config --global credential.helper 'store'`

Check here for the credentials https://github.com/go-gitea/gitea/issues/8367
`~/.git-credentials`

## Update binaries

https://github.com/go-gitea/gitea/issues/8367
